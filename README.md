## 介绍
基于Vue3、TypeScript、Element Plus、Vue Router、Pinia、Axios、i18n、Vite等开发的后台管理，使用门槛极低，采用MIT开源协议，完全免费开源且终生免费，可免费用于商业项目等场景！

## 安装
注意：需使用 nodejs 长期维护版本，如：[12.x、14.x、16.x]，能保证项目的稳定运行。

```bash
# 克隆项目
git clone https://gitee.com/makunet/fast-admin.git

# 进入项目
cd fast-admin

# 安装依赖
npm install

# 运行项目
npm run dev

# 打包发布
npm run build
```

## 后端
FastBoot：https://gitee.com/makunet/fast-boot

## 支持
如果觉得框架还不错，或者已经在使用了，希望你可以去 [Gitee](https://gitee.com/makunet/fast-admin) 帮作者点个 ⭐ Star，这将是对作者极大的鼓励与支持。

## 效果图
![输入图片说明](public/images/1.png)

![输入图片说明](public/images/2.png)

![输入图片说明](public/images/3.png)

![输入图片说明](public/images/4.png)

![输入图片说明](public/images/5.png)


